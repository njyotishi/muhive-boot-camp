package main

/**
 * Created with IntelliJ IDEA.
 * User: nehajyotishi
 * Date: 6/6/13
 * Time: 3:23 PM
 * To change this template use File | Settings | File Templates.
 */
//program header
println("This Program reads a file and prints 5 largest words")

//setting the path for file to be read
myFileDirectory = "//home//nehajyotishi//IdeaProjects//ex1//out//production//ex1//main//"
myFileName = "sample.txt"
myFile = myFileDirectory + myFileName

//defining variable and signing in
def wordList = []
def ctr = 0
def hkey = 0
def uwordList = []

def wtoken = new MyTokenizer()
if(wtoken == 0)
{
    println("File path for processing not passed correctly")
}
else
{
    wordList = wtoken.tokenizeFile(myFile)

//preparing unique list of words
    uwordList = wordList.unique()

//computing the largest size among the words
    uwordList.each { word ->
        if(hkey < word.size())
            hkey = word.size()
    }

    def range = hkey..hkey-5
    uwordList.each {
        word ->
        if(ctr!=5 && word.size()in range )
        {
            println word
            ctr += 1
        }
    }

}
