package main

/**
 * Created with IntelliJ IDEA.
 * User: nehajyotishi
 * Date: 6/6/13
 * Time: 4:13 PM
 * To change this template use File | Settings | File Templates.
 */

myFileDirectory = "//home//nehajyotishi//IdeaProjects//ex1//out//production//ex1//main//"

myFileName1 = "sample.txt"
myFile1 = myFileDirectory + myFileName1

myFileName2 = "stopwords.txt"
myFile2 = myFileDirectory + myFileName2

def wordlist1 = []
def wtoken1 = new MyTokenizer()
wordlist1 = wtoken1.tokenizeFile(myFile1)

if(wordlist1 == 0)
{
    println("File path for file processing not passed correctly")
}
else
{
    def wordlist2 = []
    def wtoken2 = new MyTokenizer()
    wordlist2 = wtoken2.tokenizeFile(myFile2)

    if(wordlist2 == 0)
    {
        println("The file path for Stop words file not passed correctly")
    }
    else
    {
        println("Size of first word list:" + wordlist1.size() )
        println( "Size of second word list:" + wordlist1.size())

        println wordlist1
        println wordlist2

        def commons = wordlist1.intersect(wordlist2)
        println commons

        def difference = wordlist1.plus(wordlist2)
        println difference

        difference.removeAll(commons)
        println difference

    }

}


