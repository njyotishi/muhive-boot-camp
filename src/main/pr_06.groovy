package main

/**
 * Created with IntelliJ IDEA.
 * User: nehajyotishi
 * Date: 6/6/13
 * Time: 1:10 PM
 * To change this template use File | Settings | File Templates.
 */

//program header
println("This Program reads a file and creates dictionary and list 5 least used words")

//setting the path for file to be read
myFileDirectory = "//home//nehajyotishi//IdeaProjects//ex1//out//production//ex1//main//"
myFileName = "sample.txt"
myFile = myFileDirectory + myFileName

//defining variable and signing in
def wordList = []
def ctr = 0
def lowkey = 0
def wordMap = [:].withDefault {0}
def wordMap1 = [:].withDefault {0}

def wtoken
wtoken = new MyTokenizer()

if(wtoken == 0)
{
    println("The file path for Sample file not passed correctly")
}
else
{
    wordList = wtoken.tokenizeFile(myFile)

    wordList.each {word -> wordMap[word]++}
    wordMap = wordMap.sort {it.value}
    println wordMap

    wordMap.each
            {
                it ->

                if (ctr!=5)
                {
                    if(lowkey < it.value)
                    {
                        lowkey = it.value
                        ctr += 1
                    }
                }
            }

    wordMap1 = wordMap.findAll {k,v -> v <=lowkey }
    println wordMap1

}




//TODO: GROOVIER version of this







