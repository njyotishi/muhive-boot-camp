package main

/**
 * Created with IntelliJ IDEA.
 * User: nehajyotishi
 * Date: 6/5/13
 * Time: 2:27 PM
 * To change this template use File | Settings | File Templates.
 * This is a program to create a list of integers between 1 and 5000 which are multiples of 5
 */
println("Program creates a list of integers between 1 and 5000 which are multiples of 5")

//creating and initializing the list
myList =[]
println ("Size of the List before computation:" + myList.size())

//loop to create the list that contains multiples of 5
j=0
for( i=5;i<=5000;i+=5)
{
    myList[j++] = i
}

println(" List of multiples of 5 between 1 and 5000 is")
println(myList)
println("Size of the list after computation:" + myList.size())

def divs =[]
(1..5000).each { if (it%5 == 0) divs << it  }
println divs

def findAllList = (1..5000).findAll{ it -> it%5==0}
println findAllList

/*
def multiples = (1..5).collect{ it*5}
              println multiples
*/
