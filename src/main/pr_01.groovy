package main

/**
 * Created with IntelliJ IDEA.
 * User: nehajyotishi
 * Date: 6/5/13
 * Time: 5:01 PM
 * To change this template use File | Settings | File Templates.
 */

println( "Program to print series of numbers between 100 and 200")

for(i = 100; i<=200; i++)
{
    println i
}

(100..200).each{ int num -> print num+'\n' }
