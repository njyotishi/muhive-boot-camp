package main

/**
 * Created with IntelliJ IDEA.
 * User: nehajyotishi
 * Date: 6/10/13
 * Time: 11:00 AM
 * To change this template use File | Settings | File Templates.
 */

 //recursive function for Fibonaaci series
def fib(int num){
    if(num== 0)  return 0
    if(num == 1 ) return 1
    else {
        return fib(num-1) + fib(num-2)

    }
}

def x = 6

def fibnum = fib(x)
println "Fibonacci Number for $x position:" + fibnum

//TODO: Print the list of fibonacci numbers instead of the nth fibo number

//recursive closure to compute fibonacci series
def rfib
rfib = { it < 1 ? 0 : it == 1 ? 1 : rfib(it-1) + rfib(it-2) }

//preparing the list of fibonacci numbers upto the given position
def fblist = []
def n = 6

(0..n).each {fblist.addAll("${rfib(it)}")}
println "Fibonacci series upto $n position is:" + fblist

