package main

/**
 * Created with IntelliJ IDEA.
 * User: nehajyotishi
 * Date: 6/5/13
 * Time: 11:11 AM
 * To change this template use File | Settings | File Templates.
 */
/*println "Trying out lists"
myList = [11, 14, 19, 22, 56, 78]

println myList

println myList [2]
println myList.size()

myList1 = ["neha", "prateeksha", "ritesh", "piyush"]
println myList1
println myList1[3]
println myList1.size()

println()

println "Trying out Maps"

iqscore = ["Neha":57, "Prateeksha":"Topper", "Ritesh":98.99]
println iqscore
println iqscore["Prateeksha"]
println iqscore.Ritesh

iqscore["Prateeksha"] = 100
println "New Score for Prateeksha"
println iqscore.Prateeksha

println ("Size of iqscore Map =" + iqscore.size())

println()
*/

/*println("Trying out conditionals")
amPM = Calendar.AM_PM
if(amPM == "AM")
    println("Good Morning")
else
    println("Good Evening")

println()
println("Another Way")
amPM2 = Calendar.getInstance().get(Calendar.AM_PM)
if(amPM2 == Calendar.AM)
    println("Good Morning")
else
    println("Good Evening")
*/

/*println()
println("Trying out Closures")
square = {it *it}
x = square(3)
println("3 * 3 = " + x)

println ("Using Collect")
myList = [1,2,3,4].collect(square)
println myList

println("Collect another way")
myList1 = [5,6,7,8]
myList2 = myList1.collect(square)
println myList2

println("Using Each")

println("Using on Text Values")
printMapClosure = { key, value -> println key + "=" + value }
[ "Yue" : "Wu", "Mark" : "Williams", "sudha" : "Kumari" ].each(printMapClosure)

fullString = ""
orderParts = ["BUY", 200, "Hot Dogs", "1"]
orderParts.each {
    fullString += it + " "
}

println fullString

myMap = ["China": 1 , "India" : 2, "USA" : 3]

result = 0
myMap.keySet().each( { result+= myMap[it] } )
println result
*/

/*println("Create a list of 10 integers")
integerList = []
println integerList.size()
i=0
for(j=1;j<=10;j++)
{
    integerList[i++] = j
}
println integerList
println integerList.size()
*/

BufferedReader br = new BufferedReader(new InputStreamReader(System.in))

println"Please enter number:"
input = br.readLine()

print "You entered: $input"


