package main

/**
 * Created with IntelliJ IDEA.
 * User: nehajyotishi
 * Date: 6/6/13
 * Time: 4:43 PM
 * To change this template use File | Settings | File Templates.
 */

myFileDirectory = "//home//nehajyotishi//IdeaProjects//ex1//out//production//ex1//main//"

myFileName1 = "sample.txt"
myFile1 = myFileDirectory + myFileName1

def wordlist1 = []
def wtoken1 = new MyTokenizer()
wordlist1 = wtoken1.tokenizeFile(myFile1)

myFileName2 = "stopwords.txt"
myFile2 = myFileDirectory+myFileName2

def wordlist2 = []
def wtoken2 = new MyTokenizer()
wordlist2 = wtoken2.tokenizeFile(myFile2)


def wordsInFileMap = [:].withDefault {0}
wordlist1.each {word -> wordsInFileMap[word]++}
wordsInFileMap.sort {it.value}

def stopWordsMap = [:].withDefault {0}
wordlist2.each {word ->stopWordsMap[word]++}
stopWordsMap.sort{it.value}

println wordsInFileMap
println stopWordsMap
def stopWordsInFile = wordsInFileMap.findAll {  stopWordsMap.containsKey(it.key)}

def keySet = wordsInFileMap.keySet()
def keySet1 = stopWordsMap.keySet()

def intersect = keySet.intersect(keySet1)
keySet.retainAll(intersect)
//print wordsInFileMap

assert wordsInFileMap.size() == stopWordsInFile.size()
/*

def commonMap = [:].withDefault {0}

commonMap = wordsInFileMap.each {k,v -> stopWordsMap.each {key,value -> k == key}}
println commonMap

def commonMapsorted = [:].withDefault {0}
commonMapsorted = commonMap.sort {-it.value}

println("Sorted Map:")
println commonMapsorted

def ctr = 0
commonMapsorted.each
        {
            it ->
           while (ctr!=5)
           {
               println it
               ctr += 1
           }
        }


*/
