package main

/**
 * Created with IntelliJ IDEA.
 * User: nehajyotishi
 * Date: 6/6/13
 * Time: 11:02 AM
 * To change this template use File | Settings | File Templates.
 */

//program header
println("This Program reads a file and counts the number of unique words in it")

//setting the path for file to be read
myFileDirectory = "//home//nehajyotishi//IdeaProjects//ex1//out//production//ex1//main//"
myFileName = "sample.txt"
myFile = myFileDirectory + myFileName

def wordList = []
def uwordList = []

def wtoken
wtoken = new MyTokenizer()

if(wtoken == 0)
{
    println("File name not passed correctly")
}
else
{
    wordList = wtoken.tokenizeFile(myFile)
    println("The unique list of words")

    uwordList = wordList
    uwordList.unique()
    println uwordList

}


