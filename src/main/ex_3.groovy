package main

/**
 * Created with IntelliJ IDEA.
 * User: nehajyotishi
 * Date: 6/10/13
 * Time: 12:11 PM
 * To change this template use File | Settings | File Templates.
 */

/*def x = [1, 2, 3]
println x
def y = 5;

x = y + 7
println x

assert x == 12 */

def x = [1, 2, 3,
        4, 5]
println(
        x
)
if (x != null &&
        x.size() > 5) {
    println("Works!")
}
else {
    assert false: "should never happen ${x}"
}
