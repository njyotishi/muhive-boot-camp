package test;

import main.MyTokenizer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: nehajyotishi
 * Date: 6/10/13
 * Time: 6:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class MyTokenizerTest {

    private MyTokenizer myTokenizer;

    @Before
    public void setUp() throws Exception
    {
        myTokenizer = new MyTokenizer();
    }

    @After
    public void tearDown() throws Exception
    {

    }

    @Test
    public void testTokenizeText() throws Exception
    {
        assert myTokenizer.tokenizeFile(null) == 0
        assert myTokenizer.tokenizeText("this is a test").size() == 4
        assert myTokenizer.tokenizeText("this is, a test").size() == 4
        assert myTokenizer.tokenizeText("this is : a; test").size() == 4
        assert myTokenizer.tokenizeText("this is : a test- for our system").size() == 7
        assert myTokenizer.tokenizeText("This is A test")== myTokenizer.tokenizeText("this is a test")
        assert myTokenizer.tokenizeText("THIS IS A TEST") == myTokenizer.tokenizeText("this is a  test")
    }
}
